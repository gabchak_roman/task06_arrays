package com.gabchak.logicalTasks.task4;

import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class DequeTest extends TestCase{

    ///////////////////////////////////////////////////////////////////////////
    // Fields
    ///////////////////////////////////////////////////////////////////////////

    // Statics
    private final static int MAX_PROBLEM_SIZE = 10000;

    // Members
    private CustomDeque<String> queue;

    // Constructors

    @Before
    public void setUp() throws Exception {
        queue = new CustomDeque<String>();
    }

    @After
    public void tearDown() throws Exception {
        queue = null;
    }

    ///////////////////////////////////////////////////////////////////////////
    // Test
    ///////////////////////////////////////////////////////////////////////////

    @Test
    public void testDeque() {
        assertNotNull( queue );
    }

    @Test
    public void testIsEmpty() {
        assertTrue("Initialized queue should be empty", queue.isEmpty());
    }

    @Test
    public void testIsEmptyAfterAddRemoveFirst() {
        queue.addFirst("Something");
        boolean empty = queue.isEmpty();
        assertFalse( empty );
        queue.removeFirst();

        empty = queue.isEmpty();
        assertTrue("Should be empty after adding then removing",
                empty);

    }

    @Test
    public void testIsEmptyAfterAddRemoveLast() {
        queue.addLast("Something");
        assertFalse(queue.isEmpty());
        queue.removeLast();
        assertTrue("Should be empty after adding then removing",
                queue.isEmpty());

    }

    @Test
    public void testIsEmptyAfterAddFirstRemoveLast() {
        queue.addFirst("Something");
        assertFalse(queue.isEmpty());
        queue.removeLast();
        assertTrue("Should be empty after adding then removing",
                queue.isEmpty());
    }

    @Test
    public void testIsEmptyAfterAddLastRemoveFirst() {
        queue.addLast("Something");
        assertFalse(queue.isEmpty());
        queue.removeFirst();
        assertTrue("Should be empty after adding then removing",
                queue.isEmpty());
    }

    @Test
    public void testIsEmptyAfterMultipleAddRemove(){
        for(int i = 0; i < MAX_PROBLEM_SIZE; i++){
            queue.addFirst("Something");
            assertFalse("Should not be empty after " + i + " item added",
                    queue.isEmpty());
        }

        for(int i = 0; i < MAX_PROBLEM_SIZE; i++){
            assertFalse("Should not be empty after " + i + " item removed",
                    queue.isEmpty());
            queue.removeLast();
        }

        assertTrue( "Should be empty after adding and removing "
                        + MAX_PROBLEM_SIZE + " elements.",
                queue.isEmpty() );
    }

    @Test
    public void testMultipleFillAndEmpty(){
        for(int tries = 0; tries < 50; tries++){
            for(int i = 0; i < MAX_PROBLEM_SIZE; i++){
                queue.addFirst(String.valueOf(i));
            }

            assertFalse( queue.isEmpty() );
            int i = 0;
            while( !queue.isEmpty() ){
                assertEquals( String.valueOf(i), queue.removeLast() );
                i++;
            }

            assertTrue( queue.isEmpty() );

            for(int j = 0; j < MAX_PROBLEM_SIZE; j++){
                queue.addLast(String.valueOf(j));
            }

            assertFalse( queue.isEmpty() );

            i = 0;
            while( !queue.isEmpty() ){
                assertEquals( String.valueOf(i), queue.removeFirst() );
                i++;
            }

            assertTrue( queue.isEmpty() );
        }
    }

    @Test
    public void testSize() {
        assertEquals( 0, queue.size() );
        for(int i = 0; i < MAX_PROBLEM_SIZE; i++){
            queue.addFirst("Something");
            assertEquals(i+1, queue.size() );
        }

        for(int i = MAX_PROBLEM_SIZE; i > 0; i--){
            assertEquals(i, queue.size() );
            queue.removeLast();
        }

        assertEquals( 0, queue.size() );
    }

    @Test
    public void testAddFirst() {
        String[] aBunchOfString = {
                "One",
                "Two",
                "Three",
                "Four"
        };

        for(String aString : aBunchOfString){
            queue.addFirst(aString);
        }

        for(int i = aBunchOfString.length - 1; i >= 0; i--){
            assertEquals(aBunchOfString[i], queue.removeFirst());
        }
    }

    @Test
    public void testAddLast() {
        String[] aBunchOfString = {
                "One",
                "Two",
                "Three",
                "Four"
        };

        for(String aString : aBunchOfString){
            queue.addLast(aString);
        }

        for(int i = aBunchOfString.length - 1; i >= 0; i--){
            assertEquals(aBunchOfString[i], queue.removeLast());
        }
    }

    @Test
    public void testRemoveFirst() {
        for(int i = 0; i < MAX_PROBLEM_SIZE; i++){
            queue.addFirst( String.valueOf(i) );
            assertEquals(String.valueOf(i), queue.removeFirst());
        }

        queue = new CustomDeque<String>();

        for(int i = 0; i < MAX_PROBLEM_SIZE; i++){
            queue.addLast( String.valueOf(i) );
            assertEquals(String.valueOf(i), queue.removeFirst());
        }

        queue = new CustomDeque<String>();

        for(int i = 0; i < MAX_PROBLEM_SIZE; i++){
            queue.addLast( String.valueOf(i) );
        }

        for(int i = 0; i < MAX_PROBLEM_SIZE; i++){
            assertEquals(String.valueOf(i), queue.removeFirst());
        }

    }

    @Test
    public void testRemoveLast() {
        for(int i = 0; i < MAX_PROBLEM_SIZE; i++){
            queue.addFirst( String.valueOf(i) );
            assertEquals(String.valueOf(i), queue.removeLast());
        }

        queue = new CustomDeque<String>();

        for(int i = 0; i < MAX_PROBLEM_SIZE; i++){
            queue.addLast( String.valueOf(i) );
            assertEquals(String.valueOf(i), queue.removeLast());
        }

        queue = new CustomDeque<String>();

        for(int i = 0; i < MAX_PROBLEM_SIZE; i++){
            queue.addFirst( String.valueOf(i) );
        }

        for(int i = 0; i < MAX_PROBLEM_SIZE; i++){
            assertEquals(String.valueOf(i), queue.removeLast());
        }
    }

    @Test
    public void testRemoveEmpty() {
        try {
            assertTrue(queue.isEmpty());
            queue.removeFirst();
            fail("Expected a NoSuchElementException");
        } catch ( NoSuchElementException nsee){
            // Continue
        } catch ( Exception e ){
            fail( "Unexpected exception : " + e );
        }

        try {
            assertTrue(queue.isEmpty());
            queue.removeLast();
            fail("Expected a NoSuchElementException");
        } catch ( NoSuchElementException nsee){
            // Continue
        } catch ( Exception e ){
            fail( "Unexpected exception : " + e );
        }

        try {
            assertTrue(queue.isEmpty());

            for(int i = 0; i < MAX_PROBLEM_SIZE; i ++ ){
                queue.addLast( String.valueOf(i) );
            }
            for(int i = 0; i < MAX_PROBLEM_SIZE; i ++ ){
                queue.removeLast();
            }
            queue.removeLast();
            fail("Expected a NoSuchElementException");
        } catch ( NoSuchElementException nsee){
            // Continue
        } catch ( Exception e ){
            fail( "Unexpected exception : " + e );
        }
    }

    @Test
    public void testIterator() {

        Iterator<String> anIterator = queue.iterator();
        assertFalse( anIterator.hasNext() );

        for(int i = 0; i < MAX_PROBLEM_SIZE; i++){
            queue.addFirst( String.valueOf(i) );
        }

        anIterator = queue.iterator();

        assertTrue( anIterator.hasNext() );

        int i = MAX_PROBLEM_SIZE - 1;
        for(String aString : queue){
            assertEquals( String.valueOf(i), aString);
            i--;
        }

        anIterator = queue.iterator();

        assertTrue( anIterator.hasNext() );

        int j = MAX_PROBLEM_SIZE - 1;
        while( anIterator.hasNext() ){
            assertEquals( String.valueOf(j), anIterator.next());
            j--;
        }
    }

    @Test
    public void testIteratorNoMoreItem() {
        Iterator<String> anIterator = queue.iterator();
        while( anIterator.hasNext() ){
            anIterator.next();
        }
        try {
            anIterator.next();
            fail( "Should have thrown a NoSuchElementException.");
        } catch( NoSuchElementException nsee ){
            // Continue
        } catch( Exception e ){
            fail( "Should have thrown a NoSuchElementException, but received" +
                    " : " + e);
        }
    }

    @Test
    public void testIteratorRemoveNotSupported() {
        Iterator<String> anIterator = queue.iterator();
        try {
            anIterator.remove();
            fail("Should have thrown an UnsupportedOperationException");
        } catch ( Exception uoe ){
            // Continue
        }
    }

    @Test
    public void testMultipleIterator(){
        for(int i = 0; i < MAX_PROBLEM_SIZE/1000; i++){

            queue = new CustomDeque<String>();
            for(int j = 0; j < i; j++){
                queue.addLast( String.valueOf(j) );
            }

            @SuppressWarnings("rawtypes")
            Iterator[] someIterators = {
                    queue.iterator(),
                    queue.iterator(),
                    queue.iterator(),
                    queue.iterator(),
                    queue.iterator(),
                    queue.iterator()
            };

            @SuppressWarnings("unchecked")
            Iterator<String>[] manyStringIterators =
                    (Iterator<String>[]) someIterators;

            for(int iterID = 0; iterID < manyStringIterators.length; iterID++){
                int index = 0;
                while( manyStringIterators[iterID].hasNext() ){
                    assertEquals( "Iterator #" + iterID + " failed:\n",
                            String.valueOf(index),
                            manyStringIterators[iterID].next());
                    index++;
                }
            }

        }
    }

    @Test
    public void testQueueBehavior(){

        String[] aBunchOfString = {
                "One", "Two", "Three", "Four"
        };

        for(String aString : aBunchOfString){
            queue.addFirst(aString);
        }

        for(String aString : aBunchOfString){
            assertEquals(aString, queue.removeLast());
        }
    }

    @Test
    public void testStackBehavior(){

        String[] aBunchOfString = {
                "One", "Two", "Three", "Four"
        };

        for(String aString : aBunchOfString){
            queue.addFirst(aString);
        }

        for(int i = aBunchOfString.length - 1; i >= 0; i--){
            assertEquals(aBunchOfString[i], queue.removeFirst());
        }
    }
}