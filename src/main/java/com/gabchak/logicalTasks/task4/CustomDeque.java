package com.gabchak.logicalTasks.task4;

import java.util.*;

public class CustomDeque<T> implements Deque<T> {
    private List<T> data;

    public CustomDeque() {
        data = new ArrayList<>();
    }

    @Override
    public void addFirst(T t) {
        data.add(0, t);
    }

    @Override
    public void addLast(T t) {
        data.add(t);
    }

    @Override
    public boolean offerFirst(T t) {
        addFirst(t);
        return true;
    }

    @Override
    public boolean offerLast(T t) {
        addLast(t);
        return true;
    }

    @Override
    public T removeFirst() {
        if (data.isEmpty()) {
            throw new NoSuchElementException();
        }
        return data.remove(0);
    }

    @Override
    public T removeLast() {
        if (data.isEmpty()) {
            throw new NoSuchElementException();
        }
        return data.remove(data.size() - 1);
    }

    @Override
    public T pollFirst() {
        if (data.isEmpty()) {
            return null;
        }
        return data.remove(0);
    }

    @Override
    public T pollLast() {
        if (data.isEmpty()) {
            return null;
        }
        return data.remove(data.size() - 1);
    }

    @Override
    public T getFirst() {
        if (data.isEmpty()) {
            throw new NoSuchElementException();
        }
        return data.get(0);
    }

    @Override
    public T getLast() {
        if (data.isEmpty()) {
            throw new NoSuchElementException();
        }
        return data.get(data.size() - 1);
    }

    @Override
    public T peekFirst() {
        if (data.isEmpty()) {
            return null;
        }
        return data.get(0);
    }

    @Override
    public T peekLast() {
        if (data.isEmpty()) {
            return null;
        }
        return data.get(data.size() - 1);
    }

    @Override
    public boolean removeFirstOccurrence(Object o) {
        return data.remove(o);
    }

    @Override
    public boolean removeLastOccurrence(Object o) {
        for (int i = data.size(); i >= 0; i--) {
            if (isEquals(data.get(i), o)) {
                data.remove(i);
                return true;
            }
        }
        return false;
    }

    private boolean isEquals(Object t, Object o) {
        if (t == o) return true;
        if (o == null && t != null) return false;
        return o.equals(t);
    }

    @Override
    public boolean add(T t) {
        return data.add(t);
    }

    @Override
    public boolean offer(T t) {
        return data.add(t);
    }

    @Override
    public T remove() {
        return removeFirst();
    }

    @Override
    public T poll() {
        return pollFirst();
    }

    @Override
    public T element() {
        return getFirst();
    }

    @Override
    public T peek() {
        return peekFirst();
    }

    @Override
    public void push(T t) {
        addFirst(t);
    }

    @Override
    public T pop() {
        return removeFirst();
    }

    @Override
    public boolean remove(Object o) {
        return removeFirstOccurrence(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return data.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return data.addAll(c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return data.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return data.retainAll(c);
    }

    @Override
    public void clear() {
        data.clear();
    }

    @Override
    public boolean contains(Object o) {
        return data.contains(o);
    }

    @Override
    public int size() {
        return data.size();
    }

    @Override
    public boolean isEmpty() {
        return data.isEmpty();
    }

    @Override
    public Iterator<T> iterator() {
        return data.iterator();
    }

    @Override
    public Object[] toArray() {
        return data.toArray();
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return data.toArray(a);
    }

    @Override
    public Iterator<T> descendingIterator() {
        final ListIterator<T> iterator = data.listIterator(data.size());

        return new Iterator<T>() {
            @Override
            public boolean hasNext() {
                return iterator.hasPrevious();
            }

            @Override
            public T next() {
                return iterator.previous();
            }

            @Override
            public void remove() {
                iterator.remove();
            }
        };
    }
}
