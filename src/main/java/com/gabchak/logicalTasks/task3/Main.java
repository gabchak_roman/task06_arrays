package com.gabchak.logicalTasks.task3;

import com.gabchak.logicalTasks.task3.country.Country;
import com.gabchak.logicalTasks.task3.country.RandomCountry;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        int numberOfCountries = 10;
        List<Country> countriesList = RandomCountry.generate(numberOfCountries);
        Country[] countriesArray = countriesList.toArray(new Country[0]);

        System.out.println("List sorted by countries:");
        Collections.sort(countriesList);
        print(countriesList);
        System.out.println("\nArray sorted by countries:");
        Arrays.sort(countriesArray);
        print(countriesArray);

        Comparator<Country> comparatorByCapital = Comparator.comparing(Country::getCapital);

        System.out.println("\nList sorted by capitals:");
        countriesList.sort(comparatorByCapital);
        print(countriesList);
        System.out.println("\nArray sorted by capitals:");
        Arrays.sort(countriesArray, comparatorByCapital);
        print(countriesArray);

        Country country = countriesList.get(5);
        System.out.println("\nBinary search: " + country);
        System.out.println("List binary search: " +
                Collections.binarySearch(countriesList, country, comparatorByCapital));
        System.out.println("Arrays binary search: " +
                Arrays.binarySearch(countriesArray, country, comparatorByCapital));
    }

    private static void print(List<Country> list) {
        list.forEach(System.out::println);
    }

    private static void print(Country[] countries) {
        Arrays.stream(countries).forEach(System.out::println);
    }
}


