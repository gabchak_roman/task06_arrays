package com.gabchak.logicalTasks.task3.country;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomCountry {

    private static Random random = new Random();

    public static List<Country> generate(int numberOfCountries) {
        List<Country> countries = new ArrayList<>();
        for (int i = 0; i < numberOfCountries; i++) {
            int randomCountryNumber =
                    random.nextInt(CountriesContainer.COUNTRIES.length - 1);
            String country = CountriesContainer.COUNTRIES[randomCountryNumber][0];
            String capital = CountriesContainer.COUNTRIES[randomCountryNumber][1];
            countries.add(new Country(country, capital));
        }
        return countries;
    }
}

class CountriesContainer {
    static final String[][] COUNTRIES = {
            {"Ukraine", "Kiev"}, {"Poland", "Warsaw"}, {"Sweden", "Stockholm"},
            {"Germany", "Berlin"}, {"Denmark", "Copenhagen"}, {"Thailand", "Bangkok"},
            {"Finland", "Helsinki"}, {"France", "Paris"}, {"Germany", "Berlin"},
            {"Australia", "Vienna"}
    };
}

