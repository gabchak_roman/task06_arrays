package com.gabchak.logicalTasks.task3.country;

public class Country implements Comparable<Country> {
    private String country;
    private String capital;

    Country(final String country, final String capital) {
        this.country = country;
        this.capital = capital;
    }

    private String getCountry() {
        return country;
    }

    public String getCapital() {
        return capital;
    }

    @Override
    public int compareTo(Country o) {
        return country.compareTo(o.getCountry());
    }

    @Override
    public String toString() {
        return "Country: " + country.toUpperCase()
                + " - Capital: " + capital.toUpperCase();
    }
}