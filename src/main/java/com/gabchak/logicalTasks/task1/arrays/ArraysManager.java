package com.gabchak.logicalTasks.task1.arrays;

import java.util.*;

/***
 * Class ArraysManager.
 */
public class ArraysManager {

    /***
     * Method takes two arrays and return the third array consisting
     * of those elements that are present in both arrays;
     * @param firstArray array of numbers.
     * @param secondArray array of numbers.
     * @return array of numbers that ara consisting
     *      * of those elements that are present in both arrays.
     */
    static int[] Intersection(final int[] firstArray, final int[] secondArray) {
        Set<Integer> set = new LinkedHashSet<>();
        for (int firstArrayValue : firstArray) {
            for (int secondArrayValue : secondArray) {
                if (firstArrayValue == secondArrayValue) {
                    set.add(firstArrayValue);
                }
            }
        }
        return collectionToArray(set);
    }

    /***
     * @param firstArray array of numbers.
     * @param secondArray array of numbers.
     * @return array of numbers present in only one of the arrays.
     */
    static int[] Difference(final int[] firstArray, final int[] secondArray) {
        Set<Integer> set = new LinkedHashSet<>();
        boolean comparisonResult;
        for (int firstArrayValue : firstArray) {
            comparisonResult = false;
            for (int secondArrayValue : secondArray) {
                if (firstArrayValue == secondArrayValue) {
                    comparisonResult = true;
                    break;
                }
            }
            if (!comparisonResult) {
                set.add(firstArrayValue);
            }
        }
        return collectionToArray(set);
    }

    /***
     * Deleted all numbers that are repeated more than twice in the array.
     * @param array array of numbers.
     * @return array of numbers where numbers that are repeated
     *          more than twice are deleted.
     */
    static int[] removeValueThatAppearsMoreThanTwoTimes(final int[] array) {
        List<Integer> list = new ArrayList<>();
        Map<Integer, Integer> map = new LinkedHashMap<>();
        for (int value : array) {
            if (map.containsKey(value)) {
                map.put(value, map.get(value) + 1);
            } else {
                map.put(value, 1);
            }
        }
        for (Map.Entry<Integer, Integer> mapValue : map.entrySet()) {
            if (mapValue.getValue() <= 2) {
                for (int i = 0; i < mapValue.getValue(); i++) {
                    list.add(mapValue.getKey());
                }
            }
        }
        return collectionToArray(list);
    }

    /***
     * Deletes sub ordered duplicates in the array.
     * @param array array of numbers.
     * @return array of numbers where all sub ordered duplicates are deleted.
     */
    static int[] deleteSubOrderedDuplicates(final int[] array) {
        List<Integer> list = new ArrayList<>();
        for (int i = 1; i < array.length; i++) {
            if (array[i - 1] != array[i]) {
                list.add(array[i - 1]);
            }
        }
        return collectionToArray(list);
    }

    /***
     * The method takes Collection of Integers and return int [] array.
     * @param list Collection of Integers.
     * @return array of integers.
     */
    private static int[] collectionToArray(final Collection<Integer> list) {
        return list.stream().mapToInt(Number::intValue).toArray();
    }
}
