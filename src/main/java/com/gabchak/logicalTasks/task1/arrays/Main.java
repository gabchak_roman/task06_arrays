package com.gabchak.logicalTasks.task1.arrays;

import java.util.Arrays;

/***
 * Class Main.
 */
public class Main {

    /***
     * Starting point for the application.
     * @param args input data.
     */
    public static void main(final String[] args) {

        int[] arr1 = {0, 6, 6, 2, 2, 4, 8, 5, 6, 10, 17, 17, 53, 53, 53, 21, 21, 21};
        int[] arr2 = {1, 3, 5, 5, 2, 7, 9, 9, 11, 12, 20, 20, 20, 25};

        System.out.println("Integer Array 1: " + Arrays.toString(arr1));
        System.out.println("Integer Array 2: " + Arrays.toString(arr2));

        System.out.println("Elements that are in both arrays \"Intersection\": "
                + Arrays.toString(ArraysManager.Intersection(arr1, arr2)));

        System.out.println("Elements that are only in the First array \"Difference\": "
                + Arrays.toString(ArraysManager.Difference(arr1, arr2)));

        System.out.println("Remove value that appears more than two times: "
                + Arrays.toString(ArraysManager.removeValueThatAppearsMoreThanTwoTimes(arr1)));

        System.out.println("Remove sub ordered Duplicates: "
                + Arrays.toString(ArraysManager.deleteSubOrderedDuplicates(arr1)));
    }
}
