package com.gabchak.logicalTasks.task1.game.model.gameComponents;

/***
 * Class Hero.
 */
public class Hero {

    /***
     * Stores hero strength.
     */
    private int heroStrength;

    /***
     * When the constructor is called.
     * hero strength is set to 25.
     */
    public Hero() {
        final int STRENGTH = 25;
        this.heroStrength = STRENGTH;
    }

    /***
     * The method shanges Hero strength.
     * @param heroStrengthIn - it takes the value which changed hero strength.
     */
    public final void changeStrength(final int heroStrengthIn) {
        this.heroStrength += heroStrengthIn;
    }

    /***
     * The method compares the strength of
     * the hero with the monster and returns a logical value.
     * @param strength - take monster strength.
     * @return if hero is stronger returns true else false.
     */
    public final boolean hasStrength(final int strength) {
        return heroStrength >= strength;
    }
}
