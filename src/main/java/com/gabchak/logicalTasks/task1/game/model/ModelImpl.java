package com.gabchak.logicalTasks.task1.game.model;

import com.gabchak.logicalTasks.task1.game.model.exceptions.GameOverException;
import com.gabchak.logicalTasks.task1.game.model.gameComponents.Doors;
import com.gabchak.logicalTasks.task1.game.model.gameComponents.Hall;
import com.gabchak.logicalTasks.task1.game.model.gameComponents.Hero;

import java.util.ArrayList;
import java.util.List;

/***
 * Class model implements Model.
 * In the class stored logic.
 */
public class ModelImpl implements Model {

    /***
     * A new class hall object is created.
     */
    private Hall hall = new Hall();
    /***
     * A new class hero object is created.
     */
    private Hero hero = new Hero();

    /***
     * The method returns hall where all doors are stored.
     * @return hall.
     */
    @Override
    public final Hall getHall() {
        return hall;
    }

    /***
     * The method returns a sequence of doors for a best walkthrough.
     * @return list with the numbers of door.
     * @throws GameOverException If the hero is the Dead throws an Exception.
     */
    @Override
    public final List<Integer> getDoorsPathToStayAlive() throws GameOverException {
        List<Integer> doorPath = new ArrayList<>();
        int doorSize = hall.getDoors().size();
        for (int i = 0; i < doorSize; i++) {
            Doors door = hall.getDoors().get(i);
            if (door.getArtifact() != null) {
                hero.changeStrength(door.getArtifact().getStrength());
                doorPath.add(i);
            }
        }
        for (int i = 0; i < doorSize; i++) {
            Doors door = hall.getDoors().get(i);
            if (door.getMonster() != null) {
                if (!hero.hasStrength(door.getMonster().getStrength())) {
                    throw new GameOverException();
                }
                doorPath.add(i);
            }
        }
        return doorPath;
    }


    /***
     * Returns the number of doors behind which the hero expects death.
     * @return - numbers of deadly doors.
     */
    @Override
    public final int countDeadlyDoors() {
        int result = 0;
        int doorSize = hall.getDoors().size();
        for (int i = 0; i < doorSize; i++) {
            Doors door = hall.getDoors().get(i);
            if (door.getArtifact() != null) {
                hero.changeStrength(door.getArtifact().getStrength());
            }
        }
        for (int i = 0; i < doorSize; i++) {
            Doors door = hall.getDoors().get(i);
            if (door.getMonster() != null) {
                if (!hero.hasStrength(door.getMonster().getStrength())) {
                    result++;
                }
            }
        }
        return result;
    }
}
