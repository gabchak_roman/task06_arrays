package com.gabchak.logicalTasks.task1.game.controller;

/***
 * interface Controller.
 */
public interface Controller {

    /***
     * Method starts the game.
     */
    void gameStart();
}
