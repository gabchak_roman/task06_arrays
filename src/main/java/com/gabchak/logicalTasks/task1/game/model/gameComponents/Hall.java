package com.gabchak.logicalTasks.task1.game.model.gameComponents;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/***
 * Class Hall.
 * All doors are stored in the hall class.
 */
public class Hall {

    /***
     * Stores doors.
     */
    private List<Doors> doors = new ArrayList<>();

    /***
     * Random Number Generator.
     */
    private Random random = new Random();

    /***
     * When the constructor is called.
     * Randomly fills the doors list with monsters or artifacts.
     */
    public Hall() {
        fillRandom();
    }

    /***
     * Method return the list of doors.
     * @return list of doors.
     */
    public final List<Doors> getDoors() {
        return doors;
    }

    /***
     * Method randomly fills the doors list with monsters or artifacts.
     */
    private void fillRandom() {
        final int numberOfDoors = 10;
        for (int i = 0; i < numberOfDoors; i++) {
            if (random.nextBoolean()) {
                doors.add(new Doors(new Monster()));
            } else {
                doors.add(new Doors(new Artifact()));
            }
        }
    }
}
