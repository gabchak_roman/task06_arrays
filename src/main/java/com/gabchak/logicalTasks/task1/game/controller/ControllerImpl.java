package com.gabchak.logicalTasks.task1.game.controller;

import com.gabchak.logicalTasks.task1.game.model.Model;
import com.gabchak.logicalTasks.task1.game.model.exceptions.GameOverException;
import com.gabchak.logicalTasks.task1.game.view.View;

/***
 * Class ControllerImpl implements Controller.
 */
public class ControllerImpl implements Controller {

    private Model model;
    private View view;

    /***
     * Class constructor which initializing model and view.
     * @param modelIn Model.
     * @param viewIn View.
     */
    public ControllerImpl(final Model modelIn,final View viewIn) {
        this.model = modelIn;
        this.view = viewIn;
    }

    /***
     * Take the choice from the keyboard and print menu
     * on the screen.
     */
    @Override
    public void gameStart() {
        view.printMessage(" ".repeat(20) + "Welcome");
        while (true) {
            printMenu();
            int choice = view.getChoice();
            switch (choice) {
                case 1:
                    view.printInfo(model.getHall());
                    break;
                case 2:
                    view.printMessage("Deadly doors");
                    view.printMessage(model.countDeadlyDoors() + "");
                    break;
                case 3:
                    view.printMessage("Game plan");
                    try {
                        view.printMessage(
                                model.getDoorsPathToStayAlive().toString());
                    } catch (GameOverException e) {
                        view.printMessage("Game over!");
                    }
                    break;
                case 4:
                    view.printMessage("Exit.");
                    return;
                default:
                    view.printMessage("Wrong input!");
            }
        }
    }

    /***
     * Print menu on the screen.
     */
    private void printMenu() {
        view.printMessage("-".repeat(50));
        view.printMessage(" ".repeat(12) + "What do you want to do.\n"
                          + "-".repeat(50));
        view.printMessage(" 1  -  Print Hall");
        view.printMessage(" 2  -  Count Deadly doors");
        view.printMessage(" 3  -  Show game plan");
        view.printMessage(" 4  -  Exit");
        view.printMessage("-".repeat(50));
    }
}
