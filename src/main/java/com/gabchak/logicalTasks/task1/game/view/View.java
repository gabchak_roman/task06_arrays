/***
 * view.
 */
package com.gabchak.logicalTasks.task1.game.view;

import com.gabchak.logicalTasks.task1.game.model.gameComponents.Hall;

/***
 * interface View.
 */
public interface View {

    /***
     * Reade the input from the keyboard.
     */
    int getChoice();

    /***
     * Method returns the input from the keyboard.
     */
    void printMessage(String message);

    /***
     * The method receives and displays text message.
     */
    void printInfo(Hall hall);
}
