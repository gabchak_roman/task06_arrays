package com.gabchak.logicalTasks.task1.game.model.exceptions;

/***
 * When hero is dead. Game over.
 */
public class GameOverException extends Exception {
    /***
     * Message that hero is dean.
     */
    public GameOverException() {
        super("Hero is dead. Game over!");
    }

}
