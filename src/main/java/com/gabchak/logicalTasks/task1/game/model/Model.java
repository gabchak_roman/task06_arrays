package com.gabchak.logicalTasks.task1.game.model;

import com.gabchak.logicalTasks.task1.game.model.exceptions.GameOverException;
import com.gabchak.logicalTasks.task1.game.model.gameComponents.Hall;

import java.util.List;

/***
 * Model interface.
 */
public interface Model {

    /***
     * The method returns hall where all doors are stored.
     * @return hall.
     */
    Hall getHall();

    /***
     * The method returns a sequence of doors for a best walkthrough.
     * @return list with the numbers of door.
     * @throws GameOverException If the hero is the Dead throws an Exception.
     */
    List<Integer> getDoorsPathToStayAlive() throws GameOverException;

    /***
     * Returns the number of doors behind which the hero expects death.
     * @return - numbers of deadly doors.
     */
    int countDeadlyDoors();
}
