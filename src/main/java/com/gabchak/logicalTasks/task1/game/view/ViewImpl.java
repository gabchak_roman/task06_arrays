package com.gabchak.logicalTasks.task1.game.view;

import com.gabchak.logicalTasks.task1.game.model.gameComponents.Doors;
import com.gabchak.logicalTasks.task1.game.model.gameComponents.Hall;

import java.util.Scanner;

/***
 * Class ViewImpl implements View.
 * Class displays information on the screen.
 */
public class ViewImpl implements View {

    /***
     * Reade the input from the keyboard.
     */
    private static Scanner scanner = new Scanner(System.in);

    /***
     * Method returns the input from the keyboard.
     * @return input from the keyboard.
     */
    @Override
    public final int getChoice() {
        return scanner.nextInt();
    }

    /***
     * The method receives and displays text message.
     * @param message take the string message.
     */
    @Override
    public final void printMessage(final String message) {
        System.out.println(message);
    }

    /***
     * Method Displays the hall doors and what behind the door.
     * @param hall hall which stores the doors.
     */
    @Override
    public final void printInfo(final Hall hall) {
        System.out.println(" ".repeat(21) + "Hall\n" + "-".repeat(50));
        for (int i = 0; i < hall.getDoors().size(); i++) {
            Doors doors = hall.getDoors().get(i);
            if (doors.getMonster() != null) {
                System.out.println("       Door №" + i + ":    Monster     "
                        + doors.getMonster().getStrength()
                        + " strength");
            } else if (doors.getArtifact() != null) {
                System.out.println("       Door №" + i + ":    Artifact    "
                        + doors.getArtifact().getStrength()
                        + " strength");
            }
        }
    }

}

