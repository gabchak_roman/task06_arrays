package com.gabchak.logicalTasks.task1.game.model.gameComponents;

/***
 * Class Monster.
 */
public class Monster {

    /***
     * Stores monster strength.
     */
    private int strength;

    /***
     *  When the constructor is called.
     *  Set monster strength with random value.
     */
    Monster() {
        final int MAX_STRENGTH = 100;
        final int MIN_STRENGTH = 5;
        this.strength = (int)
                +(Math.random() * ((MAX_STRENGTH - MIN_STRENGTH) + 1)) + MIN_STRENGTH;
    }


    /***
     * Method Returns the value of monster strength.
     * @return monster strength value.
     */
    public final int getStrength() {
        return strength;
    }

    /***
     *  Method returns monster name and strength value.
     * @return monster info.
     */
    @Override
    public final String toString() {
        return "Monster    " + strength;
    }
}
