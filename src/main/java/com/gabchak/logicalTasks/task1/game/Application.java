package com.gabchak.logicalTasks.task1.game;


import com.gabchak.logicalTasks.task1.game.controller.Controller;
import com.gabchak.logicalTasks.task1.game.controller.ControllerImpl;
import com.gabchak.logicalTasks.task1.game.model.Model;
import com.gabchak.logicalTasks.task1.game.model.ModelImpl;
import com.gabchak.logicalTasks.task1.game.view.View;
import com.gabchak.logicalTasks.task1.game.view.ViewImpl;

/***
 * Class Application.
 */
public class Application {
    public Application() {
    }

    /***
     * Starting point for the application.
     * @param args input data.
     */
    public static void main(final String[] args) {
        View view = new ViewImpl();
        Model model = new ModelImpl();
        Controller controller = new ControllerImpl(model, view);
        controller.gameStart();
    }
}
