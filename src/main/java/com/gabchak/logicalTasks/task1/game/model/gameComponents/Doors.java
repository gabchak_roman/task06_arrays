package com.gabchak.logicalTasks.task1.game.model.gameComponents;

/***
 * Class Doors.
 * Door class stores artifact and monster.
 */
public class Doors {

    /***
     * Stores artifacts.
     */
    private Artifact artifact;

    /***
     * Stores monsters.
     */
    private Monster monster;

    /***
     *
     * @param artifactIn set artifact.
     */
    Doors(final Artifact artifactIn) {
        this.artifact = artifactIn;
    }

    /***
     *
     * @param monsterIn set monster.
     */
    Doors (final Monster monsterIn) {
        this.monster = monsterIn;
    }

    /***
     * Method returns artifact.
     * @return artifact.
     */
    public final Artifact getArtifact() {
        return artifact;
    }

    /***
     * Method returns monster.
     * @return monster.
     */
    public final Monster getMonster() {
        return monster;
    }
}
