package com.gabchak.logicalTasks.task1.game.model.gameComponents;

/***
 * Class Artifact.
 */
public class Artifact {

    /***
     * Artifact value strength.
     */
    private int strength;

    /***
     * Gives the artifact random strength.
     */
    Artifact() {
        final int MAX_STRENGTH = 80;
        final int MIN_STRENGTH = 10;
        strength = (int)
                +(Math.random() * ((MAX_STRENGTH - MIN_STRENGTH) + 1)) + MIN_STRENGTH;
    }

    /***
     * Method returns artifact strength value.
     * @return strength value.
     */
    public final int getStrength() {
        return strength;
    }

    /***
     *  Method returns artifact name and strength value.
     * @return artifact info.
     */
    @Override
    public final String toString() {
        return "Artifact " + strength;
    }
}
