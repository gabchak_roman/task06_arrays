package com.gabchak.logicalTasks.task2;

import java.util.Arrays;

public class StringContainer {

    private int countIndex = 0;
    private int size = 10;
    private String[] stringsArray = new String[size];

    public void add(String string) {
        if (size >= Integer.MAX_VALUE-10){
            System.out.println("Array has been reached Max size!");
            return;
        }
        if ((size - 1) == countIndex) {
            size += size;
            stringsArray = Arrays.copyOf(stringsArray, size);
        }
        stringsArray[countIndex++] = string;
    }

    String get(int index) {
        if (index < 0 || index > countIndex) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return stringsArray[index];
    }

    String[] getArray() {
        return Arrays.copyOf(stringsArray, countIndex);
    }

}
