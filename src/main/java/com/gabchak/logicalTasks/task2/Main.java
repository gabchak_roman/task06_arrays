package com.gabchak.logicalTasks.task2;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        StringContainer container = new StringContainer();
        List<String> list = new ArrayList<>();

        long start = System.currentTimeMillis();
        for (int i = 0; i < 1000000; i++) {
            list.add(String.valueOf(i));
        }
        long end = System.currentTimeMillis();
        printLine();
        System.out.println("Java list responding time: " + (end - start) + "ms");

        printLine();
        start = System.currentTimeMillis();
        for (int i = 0; i < 1000000; i++) {
            container.add(String.valueOf(i));
        }
        end = System.currentTimeMillis();
        System.out.println("My list responding time:   " + (end - start) + "ms");
        printLine();
    }


    private static void printLine() {
        System.out.println("-".repeat(35));
    }
}