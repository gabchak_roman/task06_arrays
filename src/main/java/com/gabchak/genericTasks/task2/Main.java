package com.gabchak.genericTasks.task2;

import java.util.Iterator;
import java.util.PriorityQueue;

public class Main {

    public static void main(String[] args) {
//        java.util.PriorityQueue<Container> queue = new java.util.PriorityQueue<>();
        PriorityQueue<Container> queue = new PriorityQueue<>();
        queue.add(new Container("B"));
        queue.add(new Container("A"));
        queue.add(new Container("C"));
        Container z = new Container("Z");
        queue.add(z);

        System.out.println(queue);
        System.out.println("Poll " + queue.poll());
        System.out.println("Poll " + queue.peek());
        System.out.println("Poll " + queue.remove(z));

        Iterator<Container> iterator = queue.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }
}

class Container implements Comparable{
    private String name;

    public Container(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int compareTo(Object o) {
        return this.name.compareTo(((Container)o).name);
    }
}
