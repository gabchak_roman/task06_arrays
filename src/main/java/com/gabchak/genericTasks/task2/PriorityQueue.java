package com.gabchak.genericTasks.task2;

import java.util.*;

public class PriorityQueue<T> implements Queue<T> {

    private TreeSet<T> data;

    public PriorityQueue() {
        data = new TreeSet<>();
    }

    public PriorityQueue(Comparator<? super T> comparator) {
        data = new TreeSet<>(comparator);
    }

    @Override
    public int size() {
        return data.size();
    }

    @Override
    public boolean isEmpty() {
        return data.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return data.contains(o);
    }

    @Override
    public Iterator<T> iterator() {
        return data.iterator();
    }

    @Override
    public Object[] toArray() {
        return data.toArray();
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return data.toArray(a);
    }

    @Override
    public boolean add(T t) {
        return data.add(t);
    }

    @Override
    public boolean remove(Object o) {
        return data.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return data.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return data.addAll((c));
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return data.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return data.retainAll(c);
    }

    @Override
    public void clear() {
        data.clear();
    }

    @Override
    public boolean offer(T t) {
        return add(t);
    }

    @Override
    public T remove() {
        if (data.isEmpty()) {
            throw new NoSuchElementException();
        }
        return poll();
    }

    @Override
    public T poll() {
        return data.pollFirst();
    }

    @Override
    public T element() {
        if (data.isEmpty()) {
            throw new NoSuchElementException();
        }
        return data.first();
    }

    @Override
    public T peek() {
        if (data.isEmpty()) {
           return null;
        }
        return data.first();
    }
}
