package com.gabchak.genericTasks.task1;
import com.gabchak.genericTasks.task1.appliances.Appliance;

import java.util.ArrayList;
import java.util.List;

/***
 * Class Container save any object which are extends Appliance.
 * @param <T> object which extends Appliance.
 */
class Container <T extends Appliance>{

    /***
     * Stores the list of appliance.
     */
    private List<? super Appliance> appliancesList = new ArrayList<>();

    /***
     * Add Appliance to the list.
     * @param appliance object which extends Appliance.
     */
    void add(T appliance) {
        appliancesList.add(appliance);
    }

    /***
     * Add all appliance to the list.
     * @param list objects list which extends Appliance.
     */
    void addAll(List<? extends T> list) {
        appliancesList.addAll(list);
    }

    /***
     * Method returns the list of appliances.
     * @return list of appliances.
     */
    List<? super T> getList() {
        return appliancesList;
    }

    /***
     * Print the list of appliances on the screen.
     */
    void printList() {
        appliancesList.forEach(System.out::println);
    }

}
