package com.gabchak.genericTasks.task1.appliances;

public class TV extends Appliance {
    public TV(final String maker, final String model, final float price) {
        super(maker, model, price);
    }
}
