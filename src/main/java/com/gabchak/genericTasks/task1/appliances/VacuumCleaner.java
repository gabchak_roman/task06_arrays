package com.gabchak.genericTasks.task1.appliances;

public class VacuumCleaner extends Appliance {
    public VacuumCleaner(final String maker, final String model, final float price) {
        super(maker, model, price);
    }
}
