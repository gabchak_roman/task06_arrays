package com.gabchak.genericTasks.task1.appliances;


public abstract class Appliance  {
    private String maker;
    private String model;
    private String type;
    private float price;

    Appliance(final String maker, final String model, final float price) {
        this.type = this.getClass().getSimpleName();
        this.maker = maker;
        this.model = model;
        this.price = price;
    }

    public final String getMaker() {
        return maker;
    }

    public final String getType() {
        return type;
    }

    public final String getModel() {
        return model;
    }

    public final float getPrice() {
        return price;
    }

    @Override
    public final String toString() {
        return    "-------- Appliance ------\n"
                + "Maker:  " + maker + "\n"
                + "Type:   " + type + "\n"
                + "Model:  " + model + "\n"
                + "Price:  " + price + " UAH\n"
                + "--------------------------\n";
    }
}
