package com.gabchak.genericTasks.task1.appliances;

public class Laptop extends Appliance {
    public Laptop(final String maker, final String model, final float price) {
        super(maker, model, price);
    }
}
