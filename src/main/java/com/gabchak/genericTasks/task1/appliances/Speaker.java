package com.gabchak.genericTasks.task1.appliances;

public class Speaker extends Appliance {
    public Speaker(final String maker, final String model, final float price) {
        super(maker, model, price);
    }
}
