package com.gabchak.genericTasks.task1;

import com.gabchak.genericTasks.task1.appliances.Appliance;
import com.gabchak.genericTasks.task1.appliances.Speaker;
import com.gabchak.genericTasks.task1.appliances.TV;
import com.gabchak.genericTasks.task1.appliances.VacuumCleaner;
import com.gabchak.genericTasks.task1.appliances.Laptop;

import java.util.ArrayList;
import java.util.List;

/***
 * Cass Main.
 */
public class Main {

    /***
     * Starting point for the application.
     * @param args input data.
     */
    public static void main(String[] args) {

        Container<Appliance> appliancesContainer = new Container<>();
        appliancesContainer.add(new Speaker("Sonos", "Play 1", 15000));
        appliancesContainer.add(new TV("Sony", "KD85XG9505BR2", 178000));
        appliancesContainer.add(new VacuumCleaner("Mile", "SGEF3", 9600));
        appliancesContainer.add(new Laptop("Asus", "G501JW", 58000));

        appliancesContainer.addAll(TVList());

        appliancesContainer.printList();

    }

    /***
     * Method return the list of random appliances.
     * @return list of appliances.
     */
    private static List<TV> TVList (){
        List<TV> TVList = new ArrayList<>();
        TVList.add(new TV("Sony", "KD85XG9505BR2", 178000));
        TVList.add(new TV("LG", "KD543235BR2", 19000));
        TVList.add(new TV("Sony", "KD85XG9505BR2", 1200));
        return TVList;
    }
}
